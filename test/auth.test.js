const should = require('chai').should();
const supertest = require('supertest');
const api = supertest('http://localhost:3000/api');

describe('Authentication', () => {
  let accessToken;

      // test with existed id and attach photo
  it('should return accessToken with code 200', (done) => {
    api.post('/Admins/login')
          .send({
            username: 'admin',
            password: '123',
          })
          .expect('Content-Type', 'application/json; charset=utf-8')
          .expect(200)
          .end((err, res) => {
            if (err) return done(err);

            const body = res.body;
            body.should.be.a('object');

            body.should.have.property('id');
            body['id'].should.be.a('string');

            body.should.have.property('ttl');
            body['ttl'].should.be.a('number');

            body.should.have.property('created');
            body['created'].should.be.a('string');

            body.should.have.property('userId');
            body['userId'].should.be.a('number');

            accessToken = body.id;

            done();
          });
  });

      // test with unauthenticated request for create container
    // eslint-disable-next-line max-len
  it('should return error with code 401 without created container', (done) => {
    api.post('/Containers')
          .send({name: 'container-4'})
          .expect('Content-Type', 'application/json; charset=utf-8')
          .expect(401)
          .end((err, res) => {
            if (err) return done(err);

            const body = res.body;
            body.should.be.a('object');
            body.should.have.property('error');
            body.error.should.be.a('object');

            body.error.should.have.property('statusCode', 401);
            body.error.should.have.property(
              'message', 'Authorization Required',
            );

            done();
          });
  });

      // test create container with accessToken
  it('should return created container', (done) => {
    api.post('/Containers')
        .set('Authorization', accessToken)
        .send({name: 'container-16'})
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          const body = res.body;
          body.should.be.a('object');

          body.should.have.property('name');
          body['name'].should.be.a('string');

          body.should.have.property('size');
          body['size'].should.be.a('number');

          body.should.have.property('atime');
          body['atime'].should.be.a('string');

          body.should.have.property('mtime');
          body['mtime'].should.be.a('string');

          body.should.have.property('ctime');
          body['ctime'].should.be.a('string');

          done();
        });
  });

  after((done) => {
    api.post('/Admins/logout')
      .set('Authorization', accessToken)
      .end((err, res) => {
        accessToken = res.body.id;

        done();
      });
  });
});
