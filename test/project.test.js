const should = require('chai').should();
const supertest = require('supertest');
const api = supertest('http://localhost:3000/api');

describe('Project model', () => {
  let accessToken;

  before((done) => {
    api.post('/Admins/login')
      .send({
        username: 'admin',
        password: '123',
      })
      .end((err, res) => {
        accessToken = res.body.id;

        done();
      });
  });

  describe('POST /:id/photos', () => {
    before((done) => {
      api.post('/containers')
        .set('Authorization', accessToken)
        .send({name: 'container-0'})
        .end(() => {
          done();
        });
    });

    // test with existed id and attach photo
    it('should return uploaded photos with code 200', (done) => {
      api.post('/Projects/0/photos')
        .set('Authorization', accessToken)
        .attach('file', `${__dirname}/fixtures/1.png`)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          const body = res.body;
          body.should.be.a('array');

          body.should.be.a('array');
          body.length.should.be.eql(1);

          body[0].should.have.property('container', 'container-0');

          body[0].should.have.property('name');
          body[0]['name'].should.be.a('string');

          done();
        });
    });

    // test with existed id and without attach photo
    it('should return error with code 400 without attached photos', (done) => {
      api.post('/Projects/0/photos')
        .set('Authorization', accessToken)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect(400)
        .end((err, res) => {
          if (err) return done(err);

          const body = res.body;
          body.should.be.a('object');
          body.should.have.property('error');
          body.error.should.be.a('object');

          body.error.should.have.property('statusCode', 400);
          body.error.should.have.property('name', 'Error');
          body.error.should.have.property(
            'message', 'Не выбрано ни одного файла для загрузки',
          );

          done();
        });
    });

    // test with negative non-existed id
    it('should return error with code 404 on negative id', (done) => {
      api.post('/Projects/-1/photos')
        .set('Authorization', accessToken)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect(404)
        .end((err, res) => {
          if (err) return done(err);

          const body = res.body;

          body.should.be.a('object');
          body.should.have.property('error');
          body.error.should.be.a('object');

          body.error.should.have.property('statusCode', 404);
          body.error.should.have.property('name', 'Error');
          body.error.should.have.property(
            'message', 'Проекта с таким id не существует',
          );

          done();
        });
    });

    // test with id as string
    it('should return error with code 400 on id as string', (done) => {
      api.post('/Projects/test/photos')
        .set('Authorization', accessToken)
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect(400)
        .end((err, res) => {
          if (err) return done(err);

          const body = res.body;

          body.should.be.a('object');
          body.should.have.property('error');
          body.error.should.be.a('object');

          body.error.should.have.property('statusCode', 400);
          body.error.should.have.property('name', 'Error');
          body.error.should.have.property(
            'message', 'Value is not a number.',
          );

          done();
        });
    });

    after((done) => {
      api.delete('/containers/container-0')
        .set('Authorization', accessToken)
        .end(() => {
          done();
        });
    });
  });

  describe('GET /:id/photos', () => {
    before((done) => {
      // container without photo
      api.post('/containers')
        .set('Authorization', accessToken)
        .send({name: 'container--1'});

      // container with photo
      api.post('/containers')
        .set('Authorization', accessToken)
        .send({name: 'container-0'})
        .end(() => {
          api.post('/Projects/0/photos')
            .set('Authorization', accessToken)
            .attach('file', `${__dirname}/fixtures/1.png`)
            .end(() => {
              done();
            });
        });
    });

    // test with existed id and attached photo
    it('should return photos with code 200', (done) => {
      api.get('/projects/0/photos')
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          const body = res.body;
          body.should.be.a('array');

          body.length.should.be.eql(1);

          body[0].should.have.property('container', 'container-0');

          body[0].should.have.property('name');
          body[0]['name'].should.be.a('string');

          done();
        });
    });

    // test with existed id and without photo
    it('should return empty array with code 200', (done) => {
      api.get('/projects/-1/photos')
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          const body = res.body;
          body.should.be.a('array');

          body.length.should.be.eql(0);

          done();
        });
    });

    after((done) => {
      api.delete('/containers/container--1')
        .set('Authorization', accessToken);
      api.delete('/containers/container-0')
        .set('Authorization', accessToken)
        .end(() => {
          done();
        });
    });
  });

  describe('GET /Projects/nearest', () => {
    before((done) => {
      // data to projects
      const test = [
        {
          name: 'kek1',
          address: '123',
          contacts: '123',
          description: 'qweqwe',
          responsible: 'asdasfasf',
          latitude: '12.12121212',
          longitude: '12.12121212',
        },
        {
          name: 'kek2',
          address: '12',
          contacts: '123',
          description: 'qweqwe',
          responsible: 'asdasfasf',
          latitude: '12.12121212',
          longitude: '12.12121212',
        },
        {
          name: 'kek3',
          address: 'sdgsdg',
          contacts: '123',
          description: 'qweqwe',
          responsible: 'asdasfasf',
          latitude: '12.12121212',
          longitude: '12.12121212',
        },
        {
          name: 'kek4',
          address: '121212',
          contacts: '123',
          description: 'qweqwe',
          responsible: 'asdasfasf',
          latitude: '10.82121212',
          longitude: '10.82121212',
        },
      ];
      api.post('/Projects')
        .set('Authorization', accessToken)
        .send(test)
        .end(() => {
          done();
        });
    });

    it('should return error with code 400 when didnt pass coordinates',
      (done) => {
        api.get('/Projects/nearest')
          .expect('Content-Type', 'application/json; charset=utf-8')
          .expect(400)
          .end((err, res) => {
            if (err) return done(err);

            const body = res.body;

            body.should.be.a('object');
            body.should.have.property('error');

            body.error.should.have.property('statusCode', 400);
            body.error.should.have.property('name', 'Error');
            body.error.should.have.property(
              'message', 'long is a required argument',
            );

            done();
          });
      });

    it('should return projects with code 200', (done) => {
      api.get('/Projects/nearest?long=12.12121212&att=12.12121212&city=12')
        .expect(200)
        .end((err, res) => {
          if (err) return done(err);

          const body = res.body;
          body.should.be.a('object');

          body.should.have.property('nearest');
          body.nearest.should.be.a('array');
          for (let i = 0; i < body.nearest.length; i++) {
            body.nearest[i].should.have.property('id');
            body.nearest[i]['id'].should.be.a('number');

            body.nearest[i].should.have.property('name');
            body.nearest[i]['name'].should.be.a('string');

            body.nearest[i].should.have.property('address');
            body.nearest[i]['address'].should.be.a('string');

            body.nearest[i].should.have.property('distance');
            body.nearest[i]['distance'].should.be.a('number');
          }

          body.should.have.property('others');
          body.others.should.be.a('array');
          for (let i = 0; i < body.others; i++) {
            body.others[0].should.have.property('id');
            body.others[0]['id'].should.be.a('number');

            body.others[0].should.have.property('name');
            body.others[0]['name'].should.be.a('string');

            body.others[0].should.have.property('address');
            body.others[0]['address'].should.be.a('string');

            body.others[0].should.have.property('distance');
            body.others[0]['distance'].should.be.a('number');
          }

          done();
        });
    });
  });

  after((done) => {
    api.post('/Admins/logout')
      .set('Authorization', accessToken)
      .end((err, res) => {
        accessToken = res.body.id;

        done();
      });
  });
});
