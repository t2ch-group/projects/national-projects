const Mocha = require('mocha');
const fs = require('fs');
const path = require('path');
const {spawn} = require('child_process');

const mocha = new Mocha();

const testDir = './test';
fs.readdirSync(testDir).filter(function(file) {
  return file !== 'index.js' && file.substring(file.length - 3) === '.js';
}).forEach(function(file) {
  mocha.addFile(
    path.join(testDir, file),
  );
});

const app = spawn('npm', ['run', 'test-backend'], {detached: true});

app.stdout.on('data', (data) => {
  if (data.toString().indexOf('listening') !== -1) {
    mocha.run(function(failures) {
      process.kill(-app.pid);
      process.exit(failures ? 1 : 0);
    });
  }
});
