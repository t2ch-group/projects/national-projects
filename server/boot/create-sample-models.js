/** * @type {Array} */
const categories = require('./seeds/category');
/** * @type {Array} */
const subcategories = require('./seeds/subcategory');
/** * @type {Array} */
const projects = require('./seeds/project');
/** * @type {Array} */
const responsibles = require('./seeds/responsible');
/** * @type {Array} */
const types = require('./seeds/type');
/** * @type {Object} */
const admin = require('./seeds/admin');

module.exports = async function(app) {
  const Admin = app.models.Admin;

  if (process.env.NODE_ENV === 'prod') {
    await app.dataSources.postgresql.autoupdate();
    const user = await Admin.findOne({where: {username: 'admin'}});
    if (!user) {
      await Admin.create({
        username: process.env.ADMIN_USERNAME,
        password: process.env.ADMIN_PASSWORD,
      });
    }

    return;
  }
  await app.dataSources.postgresql.automigrate();

  const Category = app.models.Category;
  const Subcategory = app.models.Subcategory;
  const Responsible = app.models.Responsible;
  const Type = app.models.Type;
  const Project = app.models.Project;

  for (let i = 0; i < categories.length; i++) {
    await Category.create(categories[i]);
  }
  for (let i = 0; i < subcategories.length; i++) {
    await Subcategory.create(subcategories[i]);
  }
  await Responsible.create(responsibles);
  await Type.create(types);
  await Project.create(projects);

  await Admin.find({where: {username: 'admin'}}, async (err, user) => {
    if (err) return err;
    if (user.length == 0) {
      await Admin.create(admin);
    }
  });
};
