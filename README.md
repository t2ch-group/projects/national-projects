# national-projects

[[_TOC_]]

## Требования
1. NodeJS версии 10 и выше (npm)
2. PostgreSQL 11

## Настройка PostgreSQL (Linux)
1. Создать в PostgreSQL базу данных 'natprojs':
```
sudo -u postgres psql
postgres=# create database natprojs;
```
2. Создать пользователя 'test' с паролем '1234' и предоставить ему права:
```
postgres=# create user test with encrypted password '1234';
postgres=# grant all privileges on database natprojs to test;
```
3. Дать права суперпользователя пользователю 'test':
```
postgres=# alter user test with superuser;
```

## Настройка проекта
1. Склонировать проект:
```bash
git clone https://gitlab.com/t2ch-group/projects/national-projects.git
```
2. Перейти в папку с проектом и установить все зависимости:
```bash
cd national-projects
npm i
```
3. `*` Установить loopback-cli для удобства разработки:
```bash
npm i -g loopback-cli
```
4. `*` Установить vue-cli для удобства разработки:
```bash
npm i -g @vue/cli
```

## Запуск проекта
```bash
npm start
```

## Запуск тестов
```bash
npm test
```

## Сборка проекта
```bash
npm run build-client
```

`*` Не обязательно
