module.exports = {
  apps: [{
    name: 'National Projects',
    script: 'server/server.js',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: '',
    },
    env_production: {
      NODE_ENV: 'prod',
    },
  }],
};
