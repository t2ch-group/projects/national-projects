import axios from 'axios';

const host = `${process.env.NODE_ENV === 'prod' ? 'http://84.201.141.140' : 'http://localhost:3000'}/api`;
export const apiInstance = axios.create({
  baseURL: host,
});

export default {
  async getCategories() {
    return (await apiInstance.get(`/Categories/projects/count`)).data;
  },
  async getLatestProjects() {
    return (await apiInstance.get(`/Projects/latest?count=6`)).data;
  },
  async getNearestProjects(addressInfo) {
    return (await apiInstance.get(`/Projects/nearest?long=${addressInfo.long}&att=${addressInfo.att}&city=${addressInfo.city}`)).data;
  },
  async getCategoriesWithProjects() {
    return (await apiInstance.get(`/Categories?filter={"include":{"projects": ["subcategory", "responsible", "type"]}}`)).data;
  },
  async getProjectPhotos(id) {
    return (await apiInstance.get(`/Containers/container-${id}/files`)).data;
  },
  async getProjectInfo(id) {
    return (await apiInstance.get(`/Projects/${id}?filter={"include":["category", "subcategory", "type", "responsible"]}`)).data;
  },
  async getProjects(filter) {
    return (await apiInstance.get(`/Projects?filter=${filter}`)).data;
  },
  async getCategoryProjects(categoryId) {
    return (await apiInstance.get(`/Categories/${categoryId}/projects?filter={"include": ["subcategory", "responsible", "type"]}`)).data;
  },
  async getProjectsCount(where) {
    return (await apiInstance.get(`/Projects/count?${where ? `where=${where}` : ''}`)).data.count;
  },
  async getCategoriesWithSubcategories() {
    return (await apiInstance.get(`/Categories?filter={"include": "subcategories"}`)).data;
  },
  async getResponsibles() {
    return (await apiInstance.get(`/Responsibles`)).data;
  },
  async getTypes() {
    return (await apiInstance.get(`/Types`)).data;
  },

  // Admin api
  async createNewSubcategory(data) {
    return (await apiInstance.post(`/Subcategories`, data)).data;
  },
  async createNewResponsible(data) {
    return (await apiInstance.post(`/Responsibles`, data)).data;
  },
  async createNewType(data) {
    return (await apiInstance.post(`/Types`, data)).data;
  },
  async createNewProject(data) {
    return (await apiInstance.post(`/Projects`, data)).data;
  },
  async updateProject(data) {
    return (await apiInstance.put(`/Projects`, data)).data;
  },
  async deleteProject(id) {
    try {
      return (await apiInstance.delete(`/Projects/${id}`)).data;
    } catch (e) {
      return;
    }
  },
  async deleteSubcategory(id) {
    try {
      return (await apiInstance.delete(`/Subcategories/${id}`)).data;
    } catch (e) {
      return;
    }
  },
  async deleteType(id) {
    try {
      return (await apiInstance.delete(`/Types/${id}`)).data;
    } catch (e) {
      return;
    }
  },
  async deleteResponsible(id) {
    try {
      return (await apiInstance.delete(`/Responsibles/${id}`)).data;
    } catch (e) {
      return;
    }
  },
  async uploadPhotos(id, data) {
    return (await apiInstance.post(`/Projects/${id}/photos`, data, {headers: {'Content-Type': 'multipart/form-data'}})).data;
  },
  async deletePhoto(id, name) {
    return (await apiInstance.delete(`/Containers/container-${id}/files/${name}`)).data;
  },
  async auth(data) {
    return (await apiInstance.post(`/Admins/login`, data)).data;
  },
  async logout() {
    const token = localStorage.getItem('user-token');
    if (!token || !!token) return;
    return (await apiInstance.post(`/Admins/logout?access_token=${token}`)).data;
  },
};
