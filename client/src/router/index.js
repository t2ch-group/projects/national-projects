import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Search from '../views/Search';
import Project from '../views/Project';
import Projects from '../views/Projects';
import Login from '../views/Login';
import EditProjects from '../views/EditProjects';
import store from '../store';

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters['auth/isAuthenticated']) {
    next();
    return;
  }
  next('/admin/projects');
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters['auth/isAuthenticated']) {
    next();
    return;
  }
  next('/admin');
};

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: 'Главная',
    },
  },
  {
    path: '/search',
    name: 'Search',
    component: Search,
    meta: {
      title: 'Поиск проектов',
    },
  },
  {
    path: '/projects',
    name: 'AllProjects',
    component: Projects,
    meta: {
      title: 'Все полезные дела',
    },
  },
  {
    path: '/projects/:id',
    name: 'Project',
    component: Project,
    meta: {
      title: 'Страница проекта',
    },
  },
  {
    path: '/admin',
    name: 'Login',
    component: Login,
    beforeEnter: ifNotAuthenticated,
    meta: {
      title: 'Вход в панель администратора',
      isAdminPage: true,
    },
  },
  {
    path: '/admin/projects',
    name: 'Projects',
    component: EditProjects,
    beforeEnter: ifAuthenticated,
    meta: {
      title: 'Панель администратора',
      isAdminPage: true,
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
