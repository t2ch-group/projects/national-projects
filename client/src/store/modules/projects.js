import api from '../../api';

const state = {
  selectedProject: {},
  projectStatus: null,
  photos: [],
  latestProjects: [],
  latestProjectsStatus: null,
  projects: [],
  projectsCount: 0,
  projectsStatus: null,
};

const getters = {};

const actions = {
  async getLatestProjects({commit}) {
    commit('setLatestProjectsStatus', 'loading');
    const latestProjects = await api.getLatestProjects();
    commit('setLatestProjects', latestProjects);
    commit('setLatestProjectsStatus', 'success');
  },
  setSelectedProject({commit}, project) {
    commit('setSelectedProject', project);
  },
  async getPhotos({commit}, id) {
    commit('setPhotos', []);
    const photos = await api.getProjectPhotos(id);
    commit('setPhotos', photos);
  },
  async getProjectInfo({commit, rootState}, id) {
    commit('setProjectStatus', 'loading');
    const info = await api.getProjectInfo(id);
    const project = {
      id: info.id,
      name: info.name,
      icon: info.category.icon,
      color: info.category.color,
      category: info.category.name,
      subcategory: info.subcategory ? info.subcategory.name : '',
      responsible: info.responsible ? info.responsible.name : '',
      type: info.type ? info.type.name : '',
      address: info.address,
      description: info.description,
      coords: [info.latitude, info.longitude],
      endDate: info.end_date,
      email: info.email,
      website: info.website,
      phone: info.phone,
    };

    const categories = rootState.categories.categories;
    project.count = categories[info.category.id - 1].projectsCount;
    commit('setSelectedProject', project);
    commit('setProjectStatus', 'success');
  },
  async getProjects({commit}, filter) {
    commit('setProjectsStatus', 'loading');
    const projects = await api.getProjects(filter);
    commit('setProjects', projects);
    commit('setProjectsStatus', 'success');
  },
  async getProjectsCount({commit}) {
    commit('setProjectsCount', await api.getProjectsCount());
  }
};

const mutations = {
  setLatestProjectsStatus(state, status) {
    state.latestProjectsStatus = status;
  },
  setLatestProjects(state, latestProjects) {
    state.latestProjects = latestProjects;
  },
  setSelectedProject(state, project) {
    state.selectedProject = project;
  },
  setPhotos(state, photos) {
    state.photos = photos;
  },
  setProjectStatus(state, status) {
    state.projectStatus = status;
  },
  setProjects(state, projects) {
    state.projects = projects;
  },
  setProjectsStatus(state, status) {
    state.projectsStatus = status;
  },
  setProjectsCount(state, count) {
    state.projectsCount = count;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
