import api, {apiInstance} from '../../api';

const state = {
  token: localStorage.getItem('user-token') || '',
  status: localStorage.getItem('user-token') ? 'success' : '',
};

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status,
};

const actions = {
  async login({commit}, user) {
    commit('authRequest');
    try {
      const res = await api.auth(user);
      const token = res.id;
      localStorage.setItem('user-token', token);
      apiInstance.defaults.headers.common['Authorization'] = token;
      commit('authSuccess', token);
    } catch (e) {
      commit('authError');
      localStorage.removeItem('user-token');
      throw e;
    }
  },
  async logout({commit}) {
    await api.logout();
    commit('authLogout');
    localStorage.removeItem('user-token');
    delete apiInstance.defaults.headers.common['Authorization'];
  },
};

const mutations = {
  authRequest(state) {
    state.status = 'loading';
  },
  authSuccess(state, token) {
    state.token = token;
    state.status = 'success';
  },
  authError(state) {
    state.status = 'error';
    state.token = '';
  },
  authLogout(state) {
    state.status = '';
    state.token = '';
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
