import api from '../../api';

const state = {
  categories: [],
  categoriesLoadStatus: null,
};

const getters = {};

const actions = {
  async getCategories({commit}) {
    commit('setCategoriesLoadStatus', 'loading');
    const categories = await api.getCategories();
    commit('setCategories', categories);
    commit('setCategoriesLoadStatus', 'success');
  },
  async getCategoriesWithProjects() {
    return await api.getCategoriesWithProjects();
  },
};

const mutations = {
  setCategoriesLoadStatus(state, status) {
    state.categoriesLoadStatus = status;
  },
  setCategories(state, categories) {
    state.categories = categories;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
