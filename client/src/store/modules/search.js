import api from '../../api';

const state = {
  projects: {},
  searchStatus: null,
  ymapsLoaded: false,
};

const getters = {};

const actions = {
  setSearchStatus({commit}, status) {
    commit('setSearchStatus', status);
  },
  async getProjects({commit}, address) {
    const projects = await api.getNearestProjects(address);
    commit('setProjects', projects);
  },
};

const mutations = {
  setSearchStatus(state, status) {
    state.searchStatus = status;
  },
  setProjects(state, projects) {
    state.projects = projects;
  },
  setYmapsLoaded(state, status) {
    state.ymapsLoaded = status;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
