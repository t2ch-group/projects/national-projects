import Vue from 'vue';
import Vuex from 'vuex';
import projects from './modules/projects';
import categories from './modules/categories';
import search from './modules/search';
import auth from './modules/auth';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    projects,
    categories,
    search,
    auth,
  },
});
