import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import {apiInstance} from './api';

Vue.config.productionTip = false;
if (process.env.NODE_ENV === 'prod') {
  Vue.config.devtools=false;
}

const token = localStorage.getItem('user-token');
if (token) {
  apiInstance.defaults.headers.common['Authorization'] = token;
}

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
