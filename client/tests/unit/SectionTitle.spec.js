import {shallowMount} from '@vue/test-utils';
import SectionTitle from '@/components/SectionTitle.vue';

// eslint-disable-next-line
const should = require('chai').should();

function getMountedComponent(Component, propsData, slots) {
  return shallowMount(Component, {
    propsData,
    slots,
  });
}

describe('SectionTitle', () => {
  it('renders props.title when passed', () => {
    const title = 'Hello';
    const wrapper = getMountedComponent(SectionTitle, {title});

    wrapper.find('.section-title span').text().should.eql(title);
  });

  it('doesnt render line when props.no-line passed', () => {
    const title = 'Hello';
    const wrapper = getMountedComponent(SectionTitle, {title, noLine: true});

    wrapper.find('.section-title').classes().indexOf('section-title-line').should.eql(-1);
  });

  it('renders slot when passed', () => {
    const icon = {
      name: 'icon',
      template: `<img
            src="@/assets/img/useful-icon-2.png"
          />`,
    };
    const wrapper = getMountedComponent(SectionTitle, {}, {default: icon});
    wrapper.find('.section-title span').contains('img').should.eql(true);
  });
});
