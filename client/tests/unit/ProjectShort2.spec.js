import {shallowMount} from '@vue/test-utils';
import ProjectShort2 from '@/components/ProjectShort2.vue';

// eslint-disable-next-line
const should = require('chai').should();

function getMountedComponent(Component, propsData, slots) {
  return shallowMount(Component, {
    propsData,
    slots,
  });
}

describe('ProjectShort2', () => {
  it('renders props.name when passed', () => {
    const name = 'Hello';
    const wrapper = getMountedComponent(ProjectShort2, {name});

    wrapper.find('.project-title').text().should.eql(name);
  });

  it('renders props.category when passed', () => {
    const category = 'Sample';
    const wrapper = getMountedComponent(ProjectShort2, {category});

    wrapper.find('.category-name-2').text().should.eql(category);
  });

  it('renders props.icon when passed', () => {
    const icon = 'home.png';
    const wrapper = getMountedComponent(ProjectShort2, {icon});

    wrapper.find('.category-icon-2').contains('v-img-stub').should.eql(true);
  });

  it('renders props.photo when passed', () => {
    const photo = '4.png';
    const wrapper = getMountedComponent(ProjectShort2, {photo});

    wrapper.find('.project-card v-row-stub v-img-stub').exists().should.eql(true);
  });
});
