import {shallowMount} from '@vue/test-utils';
import Place from '@/components/Place.vue';

// eslint-disable-next-line
const should = require('chai').should();

function getMountedComponent(Component, propsData, slots) {
  return shallowMount(Component, {
    propsData,
    slots,
  });
}

describe('Place', () => {
  it('renders props.name when passed', () => {
    const name = 'Hello';
    const wrapper = getMountedComponent(Place, {name});

    wrapper.find('.place-card h2').text().should.eql(name);
  });

  it('renders props.icon when passed', () => {
    const icon = 'health.png';
    const wrapper = getMountedComponent(Place, {icon});

    wrapper.find('.place-card .category-icon').exists().should.eql(true);
  });

  it('renders props.subcategory when passed', () => {
    const subcategory = 'Hello';
    const wrapper = getMountedComponent(Place, {subcategory});

    wrapper.find('.place-card .subcategory-name').text().should.eql(`Региональный проект: "${subcategory}"`);
  });

  it('renders props.subcategory with long name when passed', () => {
    const subcategory = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam facilisis nec';
    const shortSubcategory = `${subcategory.substr(subcategory, 31)}...`;

    const wrapper = getMountedComponent(Place, {subcategory});

    wrapper.find('.place-card .subcategory-name').text().should.eql(`Региональный проект: "${shortSubcategory}"`);
  });

  it('renders props.responsible when passed', () => {
    const responsible = 'Hello';
    const wrapper = getMountedComponent(Place, {responsible});

    wrapper.find('.place-card .responsible-name').text().should.eql(`Ответственный: ${responsible}`);
  });

  it('renders props.address when passed', () => {
    const address = 'Hello';
    const wrapper = getMountedComponent(Place, {address});

    wrapper.find('.place-card .address').text().should.eql(address);
  });

  it('renders props.distance when passed', () => {
    const distance = 180;
    const wrapper = getMountedComponent(Place, {distance});

    wrapper.find('.place-card .distance').text().should.eql(`${distance} м`);
  });
});
