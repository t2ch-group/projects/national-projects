import {shallowMount} from '@vue/test-utils';
import Places from '@/components/Places.vue';

// eslint-disable-next-line
const should = require('chai').should();

function getMountedComponent(Component, propsData, computed, slots) {
  return shallowMount(Component, {
    propsData,
    slots,
    computed,
  });
}

describe('Places', () => {
  it('renders props.places when passed at least 1 element', () => {
    const places = [
      {
        id: 1,
        name: 'Test',
        category_id: 1,
        subcategory: 'test',
        responsible: 'test',
        address: 'test',
        distance: 800,
      },
    ];

    const computed = {
      categories: function() {
        return [
          {
            icon: 'health.png',
          },
        ];
      },
    };
    const wrapper = getMountedComponent(Places, {places}, computed);

    wrapper.findAll('#places v-row-stub').length.should.eql(1);
  });

  it('doesnt render props.places when passed empty', () => {
    const places = [];
    const wrapper = getMountedComponent(Places, {places});

    wrapper.find('#places').text().should.eql('К сожалению, ничего не найдено :(');
  });
});
