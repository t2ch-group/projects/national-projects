import {shallowMount} from '@vue/test-utils';
import CategoryStatus from '@/components/CategoryStatus.vue';

// eslint-disable-next-line
const should = require('chai').should();

function getMountedComponent(Component, propsData, slots) {
  return shallowMount(Component, {
    propsData,
    slots,
    mocks: {
      $vuetify: {
        breakpoint: {}
      }
    }
  });
}

describe('CategoryStatus', () => {
  it('renders props.title when passed', () => {
    const title = 'Hello';
    const wrapper = getMountedComponent(CategoryStatus, {title});

    const allTitles = wrapper.findAll('.category-title');
    allTitles.length.should.eql(2);
    allTitles.at(0).text().should.eql(title);
    allTitles.at(1).text().should.eql(title);
  });

  it('renders props.icon when passed', () => {
    const icon = 'health.png';
    const wrapper = getMountedComponent(CategoryStatus, {icon});

    wrapper.find('.category-icon').exists().should.eql(true);
  });

  it('renders props.count when passed', () => {
    const count = 75;
    const wrapper = getMountedComponent(CategoryStatus, {count});

    wrapper.find('.category-info > div').text().should.eql(count.toString());
    wrapper.find('.bottom-title > div').text().should.eql(count.toString());
  });
});
