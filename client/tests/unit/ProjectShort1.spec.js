import {shallowMount} from '@vue/test-utils';
import ProjectShort1 from '@/components/ProjectShort1.vue';

// eslint-disable-next-line
const should = require('chai').should();

function getMountedComponent(Component, propsData, slots) {
  return shallowMount(Component, {
    propsData,
    slots,
  });
}

describe('ProjectShort1', () => {
  it('renders props.name when passed', () => {
    const name = 'Hello';
    const wrapper = getMountedComponent(ProjectShort1, {name});

    wrapper.find('.headline').text().should.eql(name);
  });

  it('renders props.category when passed', () => {
    const category = 'Sample';
    const wrapper = getMountedComponent(ProjectShort1, {category});

    wrapper.find('.category-name').text().should.eql(category);
  });

  it('renders props.icon when passed', () => {
    const icon = 'home.png';
    const wrapper = getMountedComponent(ProjectShort1, {icon});

    wrapper.find('.category-icon').contains('v-img-stub').should.eql(true);
  });

  it('renders props.photo when passed', () => {
    const photo = '4.png';
    const wrapper = getMountedComponent(ProjectShort1, {photo});

    wrapper.find('v-img-stub').exists().should.eql(true);
  });

  it('renders props.photo when not passed', () => {
    const wrapper = getMountedComponent(ProjectShort1);

    wrapper.find('v-img-stub').exists().should.eql(true);
  });
});
