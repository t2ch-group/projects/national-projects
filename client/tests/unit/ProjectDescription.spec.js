import {shallowMount} from '@vue/test-utils';
import ProjectDescription from '@/components/ProjectDescription.vue';

// eslint-disable-next-line
const should = require('chai').should();

function getMountedComponent(Component, propsData, slots) {
  return shallowMount(Component, {
    propsData,
    slots,
  });
}

describe('ProjectDescription', () => {
  it('renders props.name when passed', () => {
    const name = 'Hello';
    const wrapper = getMountedComponent(ProjectDescription, {name});

    wrapper.find('#project-name').text().should.eql(name);
  });

  it('renders props.icon when passed', () => {
    const icon = 'health.png';
    const wrapper = getMountedComponent(ProjectDescription, {icon});

    wrapper.find('#category-icon').exists().should.eql(true);
  });

  it('renders props.color when passed', () => {
    const color = '#000000';
    const wrapper = getMountedComponent(ProjectDescription, {color});

    wrapper.find('.headline').attributes().style.should.eql(`color: rgb(0, 0, 0);`);
  });

  it('renders props.address when passed', () => {
    const address = 'Hello';
    const wrapper = getMountedComponent(ProjectDescription, {address});

    wrapper.find('#project-address').text().should.eql(address);
  });

  it('renders props.category when passed', () => {
    const category = 'Hello';
    const wrapper = getMountedComponent(ProjectDescription, {category});

    wrapper.find('.headline').text().should.eql(category);
  });

  it('renders props.subcategory when passed', () => {
    const subcategory = 'Hello';
    const wrapper = getMountedComponent(ProjectDescription, {subcategory});

    wrapper.find('#project-subcategory').text().should.eql(subcategory);
  });

  it('renders props.count when passed', () => {
    const count = 235;
    const wrapper = getMountedComponent(ProjectDescription, {count});

    wrapper.find('#projects-count').text().should.eql(`${count} полезных дел`);
  });

  it('renders props.contacts when passed', () => {
    const contacts = 'Hello';
    const wrapper = getMountedComponent(ProjectDescription, {contacts});

    wrapper.find('#project-contacts').text().should.eql(contacts);
  });

  it('renders props.responsible when passed', () => {
    const responsible = 'Hello';
    const wrapper = getMountedComponent(ProjectDescription, {responsible});

    wrapper.find('#project-responsible').text().should.eql(responsible);
  });

  it('renders props.description when passed', () => {
    const description = 'Hello';
    const wrapper = getMountedComponent(ProjectDescription, {description});

    wrapper.find('.body-2').text().should.eql(description);
  });
});
