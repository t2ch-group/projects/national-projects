import {shallowMount} from '@vue/test-utils';
import Carousel from '@/components/Carousel.vue';

// eslint-disable-next-line
const should = require('chai').should();

function getMountedComponent(Component, propsData, slots) {
  return shallowMount(Component, {
    propsData,
    slots,
  });
}

describe('Carousel', () => {
  it('renders not for fullscreen when props.fullscreen not passed', () => {
    const wrapper = getMountedComponent(Carousel);

    wrapper.find('#carousel-wrapper').exists().should.eql(false);
    wrapper.find('#photo-carousel').attributes().style.should.eql('width: 100%;');
  });

  it('renders for fullscreen when props.fullscreen passed', () => {
    const wrapper = getMountedComponent(Carousel, {fullscreen: true});

    wrapper.find('#carousel-wrapper').exists().should.eql(true);
    wrapper.find('#photo-carousel').attributes().should.not.have.property('style');
  });

  it('renders images when props.images passed', () => {
    const images = [
      '1.png',
      '2.png',
    ];
    const wrapper = getMountedComponent(Carousel, {images});

    wrapper.findAll('#photo-carousel v-carousel-item-stub').length.should.eql(2);
  });
});
