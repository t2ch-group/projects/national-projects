import {shallowMount} from '@vue/test-utils';
import Login from '@/views/Login.vue';

// eslint-disable-next-line
const should = require('chai').should();

function getMountedComponent(Component, propsData, slots) {
  return shallowMount(Component, {
    propsData,
    slots,
  });
}

describe('Login', () => {
  it('renders errors when fields are empty', async () => {
    const wrapper = getMountedComponent(Login);

    wrapper.find('#submit-btn').trigger('click');
    await wrapper.vm.$nextTick();

    wrapper.find('#login-field').attributes().errorcount.should.eql('1');
    wrapper.find('#password-field').attributes().errorcount.should.eql('1');
  });
});
