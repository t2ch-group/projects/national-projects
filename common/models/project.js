const helper = require('./helper');

module.exports = function(Project) {
  /**
   * Расчет расстояния между двумя точками.
   * @param {Object} pointA - Координаты первой точки.
   * @param {number} pointA.latitude - Широта первой точки.
   * @param {number} pointA.longitude - Долгота первой точки.
   * @param {Object} pointB - Координаты второй точки.
   * @param {number} pointB.latitude - Широта второй точки.
   * @param {number} pointB.longitude - Долгота второй точки.
   */
  Project.calculateTheDistance = (pointA, pointB) => {
    const EARTH_RADIUS = 6372795;

    // Перевод координаты в радианы
    const lat1 = pointA.latitude * Math.PI / 180;
    const lat2 = pointB.latitude * Math.PI / 180;
    const long1 = pointA.longitude * Math.PI / 180;
    const long2 = pointB.longitude * Math.PI / 180;

    // Косинусы и синусы широт и разницы долгот
    const cosLat1 = Math.cos(lat1);
    const cosLat2 = Math.cos(lat2);
    const sinLat1 = Math.sin(lat1);
    const sinLat2 = Math.sin(lat2);
    const delta = long2 - long1;
    const cosDelta = Math.cos(delta);
    const sinDelta = Math.sin(delta);

    // Вычисления длины большого круга
    const firstCathet = Math.pow(cosLat2 * sinDelta, 2);
    // eslint-disable-next-line max-len
    const secondCathet = Math.pow(cosLat1 * sinLat2 - sinLat1 * cosLat2 * cosDelta, 2);
    const y = Math.sqrt(firstCathet + secondCathet);
    const x = sinLat1 * sinLat2 + cosLat1 * cosLat2 * cosDelta;
    const ad = Math.atan2(y, x);
    const distance = Math.floor(ad * EARTH_RADIUS);

    return distance;
  };

  Project.beforeRemote('uploadImages', (ctx, output, next) => {
    const AccessToken = Project.app.models.AccessToken;

    helper.checkAuth(ctx, output, next, AccessToken);
  });

  Project.uploadImages = async function(id, ctx) {
    const storageService = Project.app.models.Container;

    const containerName = `container-${id}`;
    try {
      await storageService.getContainer(containerName);
    } catch (e) {
      const err = new Error('Проекта с таким id не существует');
      err.statusCode = 404;

      throw err;
    }

    try {
      await storageService.upload(containerName, ctx.req, ctx.res, {});
    } catch (e) {
      const err = new Error('Не выбрано ни одного файла для загрузки');
      err.statusCode = 400;

      throw err;
    }

    const uploaded = await storageService.getFiles(containerName);

    return uploaded;
  };

  Project.getImages = async function(id) {
    const storageService = Project.app.models.Container;

    const containerName = `container-${id}`;
    const files = await storageService.getFiles(containerName);

    return files;
  };

  Project.getCityProjects = async function(long, att, city) {
    const nearest = [];
    const others = [];

    // Поиск проектов, которые находятся в данном городе
    const result = await Project.find({
      where: {
        address: {
          like: `%${city}%`,
        },
      },
      include: ['subcategory', 'type', 'responsible'],
    });

    // Сортировка проектов по растоянмю от местоположения и нахождению в данном городе
    result.forEach(el => {
      const projectLocation = {
        latitude: parseFloat(el.latitude),
        longitude: parseFloat(el.longitude),
      };
      const location = {
        latitude: att,
        longitude: long,
      };

      el.distance = Project.calculateTheDistance(location, projectLocation);
      // Проверка дистанции между местоположением пользователя и проектом в данном городе
      if (el.distance <= 3000) {
        nearest.push(el);
      } else {
        others.push(el);
      }
    });

    nearest.sort((a, b) => {
      if (a.distance > b.distance) {
        return 1;
      }
      return -1;
    });

    others.sort((a, b) => {
      if (a.distance > b.distance) {
        return 1;
      }
      return -1;
    });

    const projects = {
      nearest,
      others,
    };

    return projects;
  };

  Project.getLatest = async function(count) {
    const latestProjects = await Project.find(
      {
        order: 'id DESC',
        limit: count,
      },
    );

    const getPhotos = [];
    latestProjects.forEach(project => {
      getPhotos.push(Project.getImages(project.id));
    });

    const photos = await Promise.all(getPhotos);
    photos.forEach((_photos, i) => {
      if (!_photos.length) return;

      latestProjects[i].photo = _photos[0].name;
    });

    return latestProjects;
  };

  Project.afterRemote('create', async function(ctx) {
    const storageService = Project.app.models.Container;

    await storageService.createContainer({name: `container-${ctx.result.id}`});
  });

  Project.observe('after delete', async function(ctx) {
    const storageService = Project.app.models.Container;

    try {
      await storageService.destroyContainer(`container-${ctx.where.id}`);
    } catch (e) {
    }
  });
};
