exports.checkAuth = (ctx, output, next, token) => {
  if (!ctx.req.accessToken)
    return next({message: 'Authorization Required', statusCode: 401});

  token.resolve(ctx.req.accessToken.id, (err, token) => {
    if (err)
      return next(err);

    if (!token)
      return next({message: 'Invalid Token', statusCode: 401});
    return next();
  });
};
