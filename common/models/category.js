module.exports = function(Category) {
  Category.findWithCount = async function(filter) {
    const categories = await Category.find(filter);
    const getCounts = [];
    for (let i = 0; i < categories.length; i++) {
      getCounts.push(getCount(categories, i));
    }

    await Promise.all(getCounts);

    return categories;
  };
};

function getCount(categories, index) {
  return new Promise(async (resolve) => {
    categories[index].projectsCount = await categories[index].projects.count();

    resolve();
  });
}
