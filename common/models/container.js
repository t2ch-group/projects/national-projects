const helper = require('./helper');

module.exports = function(Container) {
  Container.beforeRemote('createContainer', (ctx, output, next) => {
    const AccessToken = Container.app.models.AccessToken;

    helper.checkAuth(ctx, output, next, AccessToken);
  });

  Container.beforeRemote('destroyContainer', (ctx, output, next) => {
    const AccessToken = Container.app.models.AccessToken;

    helper.checkAuth(ctx, output, next, AccessToken);
  });

  Container.beforeRemote('removeFile', (ctx, output, next) => {
    const AccessToken = Container.app.models.AccessToken;

    helper.checkAuth(ctx, output, next, AccessToken);
  });

  Container.beforeRemote('upload', (ctx, output, next) => {
    const AccessToken = Container.app.models.AccessToken;

    helper.checkAuth(ctx, output, next, AccessToken);
  });
};
